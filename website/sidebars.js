module.exports = {
  docs: [
    {
      type: 'category',
      label: 'First Steps',
      items: [
        'getting-started',
        'wheres-stuff',
        'what-can-be-done',
      ],
    },
    {
      type: 'category',
      label: 'Troubleshooting',
      items: [
        'troubleshooting-update-stuck',
        'troubleshooting-update-not-restoring-maps',
        // 'what-can-be-done',
      ],
    },
    {
      type: 'category',
      label: 'Running arbitrary code',
      items: [
        'TBA',
        // 'wheres-stuff',
        // 'what-can-be-done',
      ],
    },
    {
      type: 'category',
      label: 'Extracting Decryption Keys',
      items: [
        'TBA',
        // 'wheres-stuff',
        // 'what-can-be-done',
      ],
    },
    {
      type: 'category',
      label: 'Decrypting updates',
      items: [
        'TBA',
        // 'wheres-stuff',
        // 'what-can-be-done',
      ],
    },
    {
      type: 'category',
      label: 'Enabling wifi + SSH',
      items: [
        'TBA',
        // 'wheres-stuff',
        // 'what-can-be-done',
      ],
    },
    {
      type: 'category',
      label: 'Disabling encryption or updates',
      items: [
        'TBA',
        // 'wheres-stuff',
        // 'what-can-be-done',
      ],
    },
    {
      type: 'category',
      label: 'Running loop from a systemd service',
      items: [
        'TBA',
        // 'wheres-stuff',
        // 'what-can-be-done',
      ],
    },
  ],
};
