---
title: Getting Started
slug: /
---

## What is all this about?

This project aims to give users ownership of the headunit on their cars. You paid for your car, you own it. That includes its computers. With this project we aim to provide you with the ability to extend your stock functionality on certain korean models. 

We are not on this for commercial gain, however, you can use whatever we've learnt so far to build and sell CFW if you wish, that is up to you. I only ask that you contribute back and share your findings. Basically, open source mentality between us here. We all want the same at the end of the day, which is completely owning our cars and the systems on it. 

* We do not sell the access to the hack. 
* We do not sell the tools to get access to the system. 
* We share our knowledge. 

You can: 
* Build and sell your CFW using the tools / knowledge that comes from collaborating. (Please, strongly consider sharing how things are done so we can attract more developers when we decide to go "public") 

Also, consider safety first. Let's try not allow non-technical people to drive while watching netflix. But this is completely to your discretion.